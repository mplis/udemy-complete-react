import { createStore } from 'redux';

console.log('starting todo example');

const stateDefault = {
    searchText: '',
    showCompleted: false,
    todos: []
};
const reducer = (state = stateDefault, action) => {
    switch (action.type) {
        case 'CHANGE_SEARCH_TEXT':
            return {
                ...state,
                searchText: action.searchText
            };
            break;
        default:
            return state;
    }
    return state;
};
const store = createStore(reducer, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());
store.subscribe(() => {
    const state = store.getState();
    console.log('Search text is', state.searchText);
    document.getElementById('app').innerHTML = state.searchText;
});

store.dispatch({
    type: 'CHANGE_SEARCH_TEXT',
    searchText: 'New search text'
});

store.dispatch({
    type: 'CHANGE_SEARCH_TEXT',
    searchText: 'More new search text'
});
