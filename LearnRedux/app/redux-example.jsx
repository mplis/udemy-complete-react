import { createStore, combineReducers } from 'redux';
import axios from 'axios';

console.log('starting redux example');

import * as actions from './actions/index';
import { configure } from './store/configureStore';

const store = configure();

const unsubscribe = store.subscribe(() => {
    const state = store.getState();

    console.log('New state', state);

    if (state.map.isFetching) {
        document.getElementById('app').innerHTML = 'Loading...';
    } else if (state.map.url) {
        document.getElementById('app').innerHTML = '<a href="' + state.map.url + '" target="_blank">View your location</a>';
    }
});
// unsubscribe();

const initialState = store.getState();
console.log('initial state', initialState);

// Action dispatching
// -----------------
store.dispatch(actions.fetchLocation());

store.dispatch(actions.changeName('Mike'));
store.dispatch(actions.changeName('Sarah'));

store.dispatch(actions.addHobby('Running'));
store.dispatch(actions.addHobby('Reading'));
store.dispatch(actions.removeHobby(2));

store.dispatch(actions.addMovie('Mad Max', 'Action'));
store.dispatch(actions.addMovie('Star Wars', 'Action'));
store.dispatch(actions.removeMovie(1));
