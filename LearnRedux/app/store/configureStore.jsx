import { createStore, combineReducers, compose, applyMiddleware } from 'redux';
import { nameReducer, hobbiesReducer, moviesReducer, mapReducer } from './../reducers/index';
import thunk from 'redux-thunk';

export const configure = () => {
    const reducer = combineReducers({
        name: nameReducer,
        hobbies: hobbiesReducer,
        movies: moviesReducer,
        map: mapReducer
    });
    const store = createStore(reducer, compose(
        applyMiddleware(thunk),
        window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__())
    );

    return store;
}
