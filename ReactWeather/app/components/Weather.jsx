var React = require('react');

var WeatherForm = require('WeatherForm');
var WeatherMessage = require('WeatherMessage');
var ErrorModal = require('ErrorModal');

var openWeatherMap = require('openWeatherMap');

var Weather = React.createClass({

  getInitialState: function() {
    return {
      isLoading: false
    };
  },

  componentDidMount: function() {
    var location = this.props.location.query.location;

    if (location && location.length > 0) {
      this.handleSearch(location);
      window.location.hash = '#/';
    }
  },

  componentWillReceiveProps: function(newProps) {
    var location = newProps.location.query.location;

    if (location && location.length > 0) {
      this.handleSearch(location);
      window.location.hash = '#/';
    }
  },

  handleSearch: function(location) {
    var that = this;

    this.setState({
      isLoading: true,
      errorMessage: undefined,
      location: undefined,
      temp: undefined
    });

    openWeatherMap.getTemp(location).then(function(temp) {
      that.setState({
        location: location,
        temp: temp,
        isLoading: false
      });
    }, function(e) {
      that.setState({
        isLoading: false,
        errorMessage: e.message
      });
    });
  },

  renderMessage: function() {
    var { location, temp, isLoading } = this.state;

    if (isLoading) {
      return <h3 className="text-center">Fetching weather...</h3>;
    } else if (temp && location) {
      return <WeatherMessage temp={temp} location={location} />;
    }
  },

  renderError: function() {
    var { errorMessage } = this.state;
    if (typeof errorMessage === 'string') {
      return (
        <ErrorModal message={errorMessage} />
      )
    }
  },

  render: function() {
    var { location, temp, isLoading } = this.state;

    return (
      <div>
        <h1 className="text-center page-title">Get Weather</h1>
        <WeatherForm onSearch={this.handleSearch} />
        {this.renderMessage()}
        {this.renderError()}
      </div>
    );
  }

});

module.exports = Weather;
