// var names = ['Mike', 'Sarah', 'Dan', 'Lauri'];

// names.forEach(function(name) {
//     console.log('forEach', name);
// });

// names.forEach((name) => {
//     console.log('arrowFunc', name);
// });

// names.forEach((name) => console.log(name));

// var returnMe = (name) => name + '!';
// console.log(returnMe('Mike'));

// var person = {
//     name: 'Mike',
//     greet: function() {
//         names.forEach((name) => {
//             console.log(this.name + ' says hi to ' + name);
//         })
//     }
// }

// person.greet();

function add(a, b) {
    return a + b;
}

var add2 = (a, b) => {
    return a + b;
}

var add3 = (a, b) => a + b;

console.log(add2(1, 3));
console.log(add2(9, 0));

console.log(add3(1, 4));
console.log(add3(10, 0));