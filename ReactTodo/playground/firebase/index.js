import firebase from 'firebase';

var config = {
    apiKey: "AIzaSyCsBFNT3b7ZpmJS7bTnbxGpFy7OuTT2Cs4",
    authDomain: "plis-todo-app.firebaseapp.com",
    databaseURL: "https://plis-todo-app.firebaseio.com",
    storageBucket: "plis-todo-app.appspot.com",
    messagingSenderId: "836327449508"
};
firebase.initializeApp(config);

const firebaseRef = firebase.database().ref();

firebaseRef.set({
    isRunning: true,
    user: {
        name: 'Mike',
        age: 25
    },
    app: {
        name: 'Todo App',
        version: '1.0.0'
    }
});

const todosRef = firebaseRef.child('todos');

todosRef.on('child_added', (snapshot) => {
    console.log('child_added', snapshot.key, snapshot.val());
});

todosRef.push({
    text: 'Foo'
});

todosRef.push({
    text: 'Bar'
});
