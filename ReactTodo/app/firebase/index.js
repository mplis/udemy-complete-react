import firebase from 'firebase';

try {
    var config = {
        apiKey: "AIzaSyCsBFNT3b7ZpmJS7bTnbxGpFy7OuTT2Cs4",
        authDomain: "plis-todo-app.firebaseapp.com",
        databaseURL: "https://plis-todo-app.firebaseio.com",
        storageBucket: "plis-todo-app.appspot.com",
        messagingSenderId: "836327449508"
    };
    firebase.initializeApp(config);
} catch (e) {
    
}

export const firebaseRef = firebase.database().ref();
export default firebase;
