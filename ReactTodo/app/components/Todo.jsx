import React from 'react';
import moment from 'moment';
import { connect } from 'react-redux';

import { startUpdateTodo } from 'actions';

export const Todo = React.createClass({

    propTypes: {
        id: React.PropTypes.string.isRequired,
        text: React.PropTypes.string.isRequired
    },

    handleTodoClick: function() {
        const { id, dispatch, completed } = this.props;
        dispatch(startUpdateTodo(id, !completed));
    },

    renderDate: function() {
        const { completed, createdAt, completedAt } = this.props;
        let timestamp = createdAt;
        let message = 'Created';
        if (completed) {
            timestamp = completedAt;
            message = 'Completed';
        }
        const formattedTime = moment.unix(timestamp).format('MMM Do YYYY @ h:mm a');
        return `${message} ${formattedTime}`;
    },

    render: function() {
        const { id, text, completed, createdAt } = this.props;
        const todoClassName = completed ? 'todo todo-completed' : 'todo';
        return (
            <div className={todoClassName} onClick={this.handleTodoClick}>
                <div>
                    <input type="checkbox" checked={completed}/>
                </div>
                <div>
                    <p>{text}</p>
                    <p className="todo-subtext">{this.renderDate()}</p>
                </div>
            </div>
        )
    }

});

export default connect()(Todo);
