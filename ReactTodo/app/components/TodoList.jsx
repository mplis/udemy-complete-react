import React from 'react';
import { connect } from 'react-redux';

import Todo from 'Todo';
import TodoAPI from 'TodoAPI';

export const TodoList = React.createClass({

    propTypes: {
        todos: React.PropTypes.array.isRequired
    },

    renderTodos: function() {
        const { todos, showCompleted, searchText } = this.props;
        if (todos.length === 0) {
            return (
                <p className="container__message">Nothing To Do</p>
            );
        }
        return TodoAPI.filterTodos(todos, showCompleted, searchText).map((todo) => {
            return (
                <Todo key={todo.id} {...todo}/>
            );
        });
    },

    render: function() {
        return (
            <div>
                {this.renderTodos()}
            </div>
        )
    }

});

export default connect(
    (state) => {
        return state;
    }
)(TodoList);
