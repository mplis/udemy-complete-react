import React from 'react';
import { connect } from 'react-redux';
import { setSearchText, toggleShowCompleted } from 'actions';

export const TodoSearch = React.createClass({

    propTypes: {
        showCompleted: React.PropTypes.bool,
        searchText: React.PropTypes.string
    },

    onSearchTextChange: function() {
        const { dispatch } = this.props;
        const searchText = this.refs.searchText.value;
        dispatch(setSearchText(searchText));
    },

    onShowCompletedChange: function() {
        const { dispatch } = this.props;
        dispatch(toggleShowCompleted());
    },

    render: function() {
        const { dispatch, showCompleted, searchText } = this.props;
        return (
            <div className="container__header">
                <div>
                    <input type="search" ref="searchText" placeholder="Search todos" value={searchText} onChange={this.onSearchTextChange}/>
                </div>
                <div>
                    <label>
                        <input type="checkbox" ref="showCompleted" checked={showCompleted} onChange={this.onShowCompletedChange}/>
                        Show completed todos
                    </label>
                </div>
            </div>
        );
    }

});

export default connect((state) => {
    return {
        showCompleted: state.showCompleted,
        searchText: state.searchText
    };
})(TodoSearch);
