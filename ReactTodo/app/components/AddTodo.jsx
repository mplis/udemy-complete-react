import React from 'react';
import { connect } from 'react-redux';
import { startAddTodo } from 'actions';

export const AddTodo = React.createClass({
    onAddTodo: function(e) {
        e.preventDefault();
        const { dispatch } = this.props;
        const todoText = this.refs.todoText.value;
        if (todoText.length > 0) {
            this.refs.todoText.value = '';
            dispatch(startAddTodo(todoText));
        } else {
            this.refs.todoText.focus();
        }
    },

    render: function() {
        return (
            <div className="container__footer">
                <form onSubmit={this.onAddTodo}>
                    <input ref="todoText" type="text" placeholder="Add Todo" />
                    <button className="button expanded" type="submit">Submit</button>
                </form>
            </div>
        );
    }
});

export default connect()(AddTodo);
