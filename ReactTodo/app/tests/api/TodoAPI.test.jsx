import expect from 'expect';

import TodoAPI from 'TodoAPI';

describe('TodoAPI', () => {

    beforeEach(() => {
        localStorage.removeItem('todos');
    });

    it('should exist', () => {
        expect(TodoAPI).toExist();
    });

    describe('setTodos', () => {
        it('should set valid todos array', () => {
            const todos = [{id: 11, text: 'Test', completed: false}];
            TodoAPI.setTodos(todos);

            const storedTodos = JSON.parse(localStorage.getItem('todos'));

            expect(storedTodos).toEqual(todos);
        });

        it('should not set invalid todos array', () => {
            const badTodos = {a: 'foobar'}
            TodoAPI.setTodos(badTodos);

            expect(localStorage.getItem('todos')).toBe(null);
        });
    });

    describe('getTodos', () => {
        it('should return empty array for bad localStorage data', () => {
            const storedTodos = TodoAPI.getTodos();

            expect(storedTodos).toEqual([]);
        });

        it('should return todos if valid array in localStorage', () => {
            const todos = [{id: 11, text: 'Test', completed: false}];
            localStorage.setItem('todos', JSON.stringify(todos));

            const storedTodos = TodoAPI.getTodos();

            expect(storedTodos).toEqual(todos);
        });
    });

    describe('filterTodos', () => {
        const todos = [
            {
                id: 1,
                text: 'Some text here',
                completed: true
            },
            {
                id: 2,
                text: 'Other text here',
                completed: false
            },
            {
                id: 3,
                text: 'Some text here',
                completed: true
            },
        ]

        it('should return all items if showCompleted is true', () => {
            const filteredTodos = TodoAPI.filterTodos(todos, true, '');

            expect(filteredTodos.length).toBe(3);
        });

        it('should not show completed todos if showCompleted is false', () => {
            const filteredTodos = TodoAPI.filterTodos(todos, false, '');

            expect(filteredTodos.length).toBe(1);
        });

        it('should sort by completed status', () => {
            const filteredTodos = TodoAPI.filterTodos(todos, true, '');

            expect(filteredTodos[0].completed).toBe(false);
        });

        it('should filter todos based on searchText', () => {
            const filteredTodos = TodoAPI.filterTodos(todos, true, 'other');

            expect(filteredTodos.length).toBe(1);
        })
    });
});
