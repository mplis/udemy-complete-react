import React from 'react';
import ReactDOM from 'react-dom';
import expect, { createSpy } from 'expect';
import $ from 'jQuery';
import TestUtils from 'react-addons-test-utils';

import { startUpdateTodo } from 'actions';

import { Todo } from 'Todo';

describe('Todo', () => {
    it('should exist', () => {
        expect(Todo).toExist();
    });

    it('should dispatch UPDATE_TODO action on click', () => {
        const todoData = {
            id: 199,
            text: 'Write test',
            completed: true
        };
        const action = startUpdateTodo(todoData.id, !todoData.completed);
        const spy = createSpy();
        const todo = TestUtils.renderIntoDocument(<Todo {...todoData} dispatch={spy} />);
        const $el = $(ReactDOM.findDOMNode(todo));

        TestUtils.Simulate.click($el[0]);

        expect(spy).toHaveBeenCalledWith(action);
    });
});
