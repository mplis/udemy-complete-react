import React from 'react';
import ReactDOM from 'react-dom';
import expect, { createSpy } from 'expect';
import $ from 'jQuery';
import TestUtils from 'react-addons-test-utils';
import { startAddTodo } from 'actions';

import { AddTodo } from 'AddTodo';

describe('AddTodo', () => {
    it('should exist', () => {
        expect(AddTodo).toExist();
    });

    it('should dispatch ADD_TODO if valid todo is entered', () => {
        const todoText = "Create Todo App";
        const action = startAddTodo(todoText);
        const spy = createSpy();
        const addTodo = TestUtils.renderIntoDocument(<AddTodo dispatch={spy} />);
        const $el = $(ReactDOM.findDOMNode(addTodo));

        addTodo.refs.todoText.value = todoText;
        TestUtils.Simulate.submit($el.find('form')[0]);

        expect(spy).toHaveBeenCalledWith(action);
    });

    it('should not dispatch ADD_TODO if empty string is entered', () => {
        const todoText = "";
        const spy = createSpy();
        const addTodo = TestUtils.renderIntoDocument(<AddTodo dispatch={spy} />);
        const $el = $(ReactDOM.findDOMNode(addTodo));

        addTodo.refs.todoText.value = todoText;
        TestUtils.Simulate.submit($el.find('form')[0]);

        expect(spy).toNotHaveBeenCalled();
    });
});
