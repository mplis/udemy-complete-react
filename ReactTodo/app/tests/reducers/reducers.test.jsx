import expect from 'expect';
import df from 'deep-freeze-strict';
import * as reducers from 'reducers';

describe('Reducers', () => {
    describe('searchTextReducer', () => {
        it('should set search text', () => {
            const action = {
                type: 'SET_SEARCH_TEXT',
                searchText: 'dog'
            };
            const res = reducers.searchTextReducer(df(''), df(action));

            expect(res).toEqual(action.searchText);
        });
    });

    describe('showCompletedReducer', () => {
        it('should toggle showCompleted status', () => {
            const action = { type: 'TOGGLE_SHOW_COMPLETED' };
            const res = reducers.showCompletedReducer(df(false), df(action));

            expect(res).toBe(true);
        });
    });

    describe('todosReducer', () => {
        it('should add new todo', () => {
            const action = {
                type: 'ADD_TODO',
                todo: {
                    id: '123',
                    text: 'something to do',
                    completed: false,
                    createdAt: 1000
                }
            };
            const res = reducers.todosReducer(df([]), df(action));

            expect(res.length).toBe(1);
            expect(res[0]).toEqual(action.todo);
        });

        it('should update todo', () => {
            const todos = [{
                id: 1,
                completed: false,
                completedAt: undefined
            }];
            const updates = {
                completed: true,
                completedAt: 300
            };
            const action = {
                type: 'UPDATE_TODO',
                id: todos[0].id,
                updates
            };
            const res = reducers.todosReducer(df(todos), df(action));

            expect(res[0].completed).toBe(updates.completed);
            expect(res[0].completedAt).toBe(updates.completedAt);
            expect(res[0].text).toBe(todos[0].text);
        });

        it('should add existing todos', () => {
            const todos = [
                {
                    id: '111',
                    text: 'anything',
                    completed: false,
                    completedAt: undefined,
                    createdAt: 3000
                }
            ];
            const action = {
                type: 'ADD_TODOS',
                todos
            };
            const res = reducers.todosReducer(df([]), df(action));

            expect(res.length).toBe(1);
            expect(res[0]).toEqual(todos[0]);
        });
    });
});
