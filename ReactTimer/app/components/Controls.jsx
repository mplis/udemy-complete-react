const React = require('react');

const Controls = React.createClass({

    propTypes: {
        status: React.PropTypes.string.isRequired,
        onStatusChange: React.PropTypes.func.isRequired
    },

    onStatusChange: function(newStatus) {
        return () => {
            this.props.onStatusChange(newStatus);
        }
    },

    renderStartStopButton: function() {
        const { status } = this.props;
        if (status === 'started') {
            return <button className="button secondary" onClick={this.onStatusChange('paused')}>Pause</button>;
        } else if (status === 'paused' || status === 'stopped') {
            return <button className="button primary" onClick={this.onStatusChange('started')}>Start</button>;
        }
    },

    render: function() {
        const { status } = this.props;
        return (
            <div className="controls">
                {this.renderStartStopButton()}
                <button className="button alert hollow" onClick={this.onStatusChange('stopped')}>Clear</button>
            </div>
        )
    }

});

module.exports = Controls;
