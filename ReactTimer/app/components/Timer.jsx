import React from 'react';

const Clock = require('Clock');
const Controls = require('Controls');

const Timer = React.createClass({

    getInitialState: function() {
        return {
            count: 0,
            timerStatus: 'paused'
        }
    },

    componentDidUpdate: function(_, prevState) {
        const { timerStatus } = this.state;
        if (prevState.timerStatus !== timerStatus) {
            switch (timerStatus) {
                case 'started':
                    this.startTimer();
                    break;
                case 'stopped':
                    this.setState({
                        count: 0
                    });
                case 'paused':
                    clearInterval(this.timer);
                    this.timer = undefined;
                    break;
            }
        }
    },

    componentWillUnmount: function() {
        clearInterval(this.timer);
    },

    startTimer: function() {
        this.timer = setInterval(() => {
            this.setState({
                count: this.state.count + 1
            });
        }, 1000);
    },

    handleStatusChange: function(newStatus) {
        this.setState({
            timerStatus: newStatus
        });
    },

    render: function() {
        const { count, timerStatus } = this.state;
        return (
            <div>
                <h1 className="page-title">Timer App</h1>
                <Clock totalSeconds={count} />
                <Controls status={timerStatus} onStatusChange={this.handleStatusChange} />
            </div>
        );
    }

});

module.exports = Timer;
