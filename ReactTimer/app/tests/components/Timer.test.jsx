const React = require('react');
const ReactDOM = require('react-dom');
const expect = require('expect');
const $ = require('jQuery');
const TestUtils = require('react-addons-test-utils');

const Timer = require('Timer');

describe('Timer', () => {
    it('should exist', () => {
        expect(Timer).toExist();
    });

    describe('handleStatusChange', () => {
        it('should start timer when status is set to started', (done) => {
            const timer = TestUtils.renderIntoDocument(<Timer />);
            timer.handleStatusChange('started');

            setTimeout(() => {
                expect(timer.state.count).toBe(2);
                expect(timer.state.timerStatus).toBe('started');
                done()
            }, 2001);
        });

        it('should pause timer when status is set to paused', (done) => {
            const timer = TestUtils.renderIntoDocument(<Timer />);
            timer.handleStatusChange('started');
            setTimeout(() => {
                timer.handleStatusChange('paused');
                setTimeout(() => {
                    expect(timer.state.count).toBe(2);
                    expect(timer.state.timerStatus).toBe('paused');
                    done();
                }, 1001);
            }, 2001);
        });

        it('should reset timer when status is set to stopped', (done) => {
            const timer = TestUtils.renderIntoDocument(<Timer />);
            timer.handleStatusChange('started');
            setTimeout(() => {
                timer.handleStatusChange('stopped');
                expect(timer.state.count).toBe(0);
                expect(timer.state.timerStatus).toBe('stopped');
                done();
            }, 1001);
        });
    });
})
